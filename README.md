Mon site est composée de 3 pages : l'acceuil , le formulaire de réservation et la liste des réservations. 
Pour faire ces pages, j'ai utilisé plusieurs fichiers php ainsi que txt ( permettant de stocker les données).

GENERAL:
	Des variables globales booléenes ont été utilisé dont l'importance va apparaitre au fur et à mesure de l'explication.
		$vare = false; 
      		$up=false; 
      		$inter =false; 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

I - Réserver une chambre

     * J'ai utilisé deux fichiers: "reservation.php": initialise le corps de notre page = formulaire avec les inputs
			           "fichier.php": crée la réservation  et stocke les données de l'utilisateur dans le fichier "reservation.txt"
     * La méthode "GET" a été adopté pour pouvoir renvoyer les données de l'utilisateur vers le "fichier.php".
     * Pour différencier les utilisateurs, j'ai utilisé un ID. 
     * Avec la function "cont()", je compte le nombre de lignes dans le fichier "reservation.txt".
     * Grace à "$line = $data[count($data)-1];" j'ai pu recupérer la dernière donnée dans le fichier (dernière ligne).
     * Les données étant stockées mainetenant dans un tableau, je prend le champ [0] du tableau (qui est l'ID) et je l'incrémente, cette étape est importante pour garantir l'incrémentation automatique de ID.
     * Par la suite, je fais l'écriture de mes données dans le fichier par concaténation. 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      
II- Recherche d'une réservation 

     * La recherche est implémenté dans le fichier "list.php". Le gestionnaire cherche dans la liste des réservations, une réservation qu'il veut: verifier, supprimer ou modifier.
           Pour cela, j'ai fais un systeme de recherhche avec des mots clés plus ou moins correcte.

     * Recherche à partir du formulaire de recherche: 
          Quand le bouton avec name="search" est cliqué, on mets la variable "$vare" à "true", cela permet d'appeler la fonction "testfun()" qui cherche dans la liste de réservation avec le mot clé inseré et retourne un tableau contenant toutes les réservations. 
	  Par la suite, la liste de réservations recherchée va etre afficher dans un tableau.

NB:J'ai gardé les deux boutons de modification et suppression dans le nouveau tableau afficher car normalement on cherche pour faire une de ces deux actions.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

III- Supression d'une réservation

      * Pour supprimer une réservation, on utilise le bouton de name="delete".
	Une fois on clique sur le bouton, on récupère l'ID de la réservation sélectionnée, on cherche la ligne suivant l'id dans le fichier des réservation grace à "strpos", on supprime cette ligne et puis on réecrit les données dans le fichier sans cette réservation.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IV- Modification d'une reservation

      * La modification d'une réservation est formé de deux étapes: la recherche des données de l'utilisation et la création d'une nouvelle donnée ayant le meme ID.

	a- Recherche des données
		
	      - Pour récuperer les donnees à modifier, comme la partie II, on utilise la fonction "testfun()" qui permet de chercher la ligne contenant la réservation et de la retourner pour les afficher dans un formulaire.
		La variable "$intere" est utilisée pour savoir si le contenu du "div" de la liste de reservation sera afficher.
		Si "$intere =true" , la liste des reservations disparait.
		Sinon, elle reste afficher.
	
	b- Création d'une nouvelle réservation ayant le meme "ID"
	      - La variable "$up" est mis à true.
		Dans ce cas, un nouveau formulaire sera afficher contenant les anciennes donnée de la réservation sélectionnée.
		Une fois on fait les modifications t on clique sur le bouton de name="modif", on récupere les nouvelles données et on les affiche dans le tableau de liste ainsi qu'on modifie notre txt.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

V- Pagination

      * Pour la pagination, 3 valeurs ont été les plus importantes : 
	- Le point de départ "start": qui est la ligne à partir de laquelle on  commence l'affichage.
	- Le nombre de lignes "size" dans le fichier des réservations: "reservation.txt".
	- Et le pas "comp": qui est le nombre de ligne à afficher 
	Ainsi que Deux boutons ont été utilisé : next et precedent.

	Bouton next: Une fois cliqué, on incrémente le start de "comp".
	Bouton precedent: Une fois cliqué, on decrémente le start de "comp".

	Deux cas ont été identifié:
		1- Si "start !=0": on affiche le bouton precedent.
		2- Si "$start + $comp < $size": on affiche le bouton next.

	NB: dans certains buttons comme "next", on définit "$start" comme valeur du button (en mode hidden), car la valeur de "$start" depend de l'action précedente. Pareil pour le button "precedent"
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Voici le lien de mon site:

	Site pedago:https://pedago.univ-avignon.fr/~uapv2201857/Miniprojet/acceuil.php (je ne suis pas sur que ca fonctionne correctement vue que je n'ai pas pu tester de chez moi).
	Gitlab: https://gitlab.com/kindachamma/mini-projet-dbweb-l1s2
		
	

		

		





