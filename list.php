<?php

        //count number of lines in "reservation.txt"
      function cont(){
          $comp=-1; 
          $handle = fopen("reservation.txt", "r");
          while(!feof($handle)){
            $line = fgets($handle);
            $comp++;       
          }
          return $comp;
      }

      $vare = false; //search results: nous permet de changer le contenu du div id="result2"
      $up=false; // update function
      $inter =false;
      // bouttons next et precd
      $comp=2; // step
      $start=0; // start point
      $size =cont(); // number of lines

      //search 
      function testfun($key){  
      
        $searchthis= $key; 
        $matches = array(); // empty array
        $handle = fopen("reservation.txt", "r");
        if ($handle)
        {
            while (!feof($handle))
            {
              
                $buffer = fgets($handle); //line
                if(strpos($buffer, $searchthis) !== FALSE){
                  $matches[] = $buffer;
                }
            }
          fclose($handle);
          return $matches;
        }
      }

      if(array_key_exists('delete',$_POST)){

        $number_l = $_POST["delete"];// id unique
        $file_out = file("reservation.txt"); // Read the whole file into an array

        foreach( $file_out as $key=>$line ) {
          if( strpos($line, $number_l) !== false) {
            unset($file_out[$key]);
          }
        }        
        //Recorded in a file
        file_put_contents("reservation.txt", implode("", $file_out));
      }
      

      if(array_key_exists("update",$_POST)){

        $row_number= $_POST["update"];  // id de la ligne
        $up=true; // up : nous permet d'afficher le formulaire avec les données qui existent
        $inter=true; // pour faire disparaitre la liste des resrvations
      }


      if(array_key_exists('search',$_POST)){
        $vare=true;
      }


      if(array_key_exists('modif',$_POST)){
          $number= $_POST["modif"]; // line number :id
          $file_out = file("reservation.txt"); // Read the whole file into an array
          $newdata = array(); // empty array
          $line= testfun($number); // la ligne à modifier

          // recuperer les nouvelles données
          $name= $_POST["name"];
          $surname= $_POST["surname"];
          $room= $_POST["size"];
          $duration= $_POST["duration"];
          $savestring = $number."|".$name."|".$surname ."|". $room ."|" .$duration."\n"; // concatenation
          
          foreach ($file_out as $filerow) {
            if (strstr($filerow, $line[0]) !== FALSE)
                $filerow = $savestring;
            $newdata[] = $filerow;
            
          }
          file_put_contents("reservation.txt", implode("", $newdata)); // save the records to the file
      }
    
      if(isset($_POST["next"])){
        $start= $_POST["next"] + $comp; 
      }

      if(isset($_POST["prec"])){
        $start= $_POST["prec"] - $comp;
      }
?>
<html>
  <head>
    <style>
          body {font-family: Arial, Helvetica, sans-serif;}

          /* The Modal (background) */
          .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
          }

          /* Modal Content */
          .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
          }

          /* The Close Button */
          .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
          }

          .close:hover,
          .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
          }
    </style>
</head>

<div class="w3-container w3-margin w3-card">  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <br></br>
    <div>
        <h1 class="w3-center"> <i class="fa fa-bars"></i>Reservations list </h1>
    </div>
    <br></br>

    <label><h4><i class="glyphicon glyphicon-search"></i> Search </h4></label> 
    <form action="" method="post">
      <table>
        <tr></tr>
          <td style="width: 270.66%">
              <input type="text" placeholder="Key.." class="w3-input" name="keyword" id="keyword" value="">
          </td>
          <td style="width: 16.66%">
              <button value="result2" name="search"  class="w3-button w3-black"><i class="fa fa-search"></i></button> 
          </td>
        </td>
      </table>
    <form>
    <br></br>

    <div id="result2">
        <?php 
            if($vare == false && $inter == false ){

                  $file = fopen("reservation.txt","r"); // r : read
                  echo '<div>
                      <form method="post" action= "" >';
                  echo " <table class='w3-table w3-bordered'>
                          <tr>
                              <th>Name</th>
                              <th>Surname</th>
                              <th>Room</th>
                              <th>Duration</th>
                          </tr>";
                      $contents = array_slice(file('reservation.txt'), $start , $comp); // remove 2 last lines
                    
                      foreach ($contents as $field_name => $line) {
                          if(sizeof(str_split($line)) < 3){
                            break;   
                          }
                          $pos=$field_name + $start;
                          list($id,$name,$surname,$room,$duration)=explode("|",$line);
                          echo"<tr>
                                  <td>$name</td>
                                  <td> $surname</td>
                                  <td> $room</td>
                                  <td> $duration</td>
                                  <td style='width: 10.66%'>
                                  <button class='w3-button w3-green' type='submit'  name='update' value='$id'> <i class='fa fa-refresh'></i></button>
                                  </td>
                                  <td style='width: 10.66%'><button  type='submit'  name='delete' value='$id' class='w3-button w3-red'> <i class ='fa fa-trash'></i></button></td>
                              </tr>";
                      }             
                  echo "</table>";
                  echo "<div class='w3-bar'>";
                  if($start!==0){ echo"<button class='w3-button w3-left w3-light-grey' name='prec' value='$start'>&laquo; Previous</button>";}
                  if($start + $comp < $size){ echo"<button class='w3-button w3-right w3-green'  name='next' value='$start'>Next &raquo;</button>";}
                  
                  echo "</div></form>
                  </div> "; 
                  
            }
            if ( $vare == true && $inter == false) {
                  $serch= testfun($_POST['keyword']);
                  //echo $serch;
                  echo "<h1 class='w3-center'> <i class='fa fa-bed'></i> Rooms </h1> </br> </br>";
                  echo " <table class='w3-table w3-bordered'>
                          <tr>
                              <th>Name</th>
                              <th>Surname</th>
                              <th>Room</th>
                              <th>Duration</th>
                          </tr>";
                      foreach ($serch as $field => $values) {               
                          list($id, $name,$surname,$room,$duration)=explode("|",$values);
                          echo"<tr>
                                  <td>$name</td>
                                  <td> $surname</td>
                                  <td> $room</td>
                                  <td> $duration</td>
                                  <td style='width: 10.66%'>
                                  <button class='w3-button w3-green' type='submit'  name='update' value='$id'> <i class='fa fa-refresh'></i></button>
                                  </td>
                                  <td style='width: 10.66%'>
                                  <button type='submit'  name='delete' value='$id' class='w3-button w3-red'> <i class ='fa fa-trash'>
                                  </i></button></td>
                              </tr>";
                  
                      } 
                      echo "</table>";
                      echo "</br> </br>";
            }
            if ( $up == true ) {
                  ?>   
                  <div class="w3-container w3-margin w3-card">  
                        <div>
                        <h4 class="w3-center"> <i class="fa fa-calendar-check-o"></i> Update </h4>
                        </div>
                        <?php 
                          $to_update = testfun($row_number); 
                          foreach ($to_update as $to_up => $row)
                          list($id,$name,$surname,$room,$duration)=explode("|",$row);
                        ?>
                        <form method="post" action="">
                          <label class="w3-text-theme ">Name</label>  
                          <input class="w3-input" id="name" type="text" name="name" value="<?php echo $name;?>"/>

                          <label class="w3-text-theme ">Surname</label>  
                          <input class="w3-input" id="surname" name="surname" type="text" value="<?php echo $surname;?>"/>
            
                          <div>
                              <label class="w3-text-theme" > Size of the room </label><br>
                                <div class="w3-half w3-container" >
                                    <input type="checkbox" name="size" value="one_p" <?php if(strcmp($room, 'one_p') == 0){echo "checked";}?> /> 1 person <br>
                                    <input type="checkbox" name="size" value="two_p" <?php if(strcmp($room, 'two_p') == 0){echo "checked";}?>/> 2 persons<br>
                                    <input type="checkbox" name="size" value="three_p" <?php if(strcmp($room, 'three_p') == 0){echo "checked";}?>/> 3 persons <br>
                                    <input type="checkbox" name="size" value="four_p" <?php if(strcmp($room, 'four_p') == 0){echo "checked";}?>/> 4 persons <br>
                                </div>
                                
                                <label class="w3-text-theme" for="activite"> Duration </label>
                                <div class="w3-half w3-container">
                                    <select class="w3-select w3-border" id="duration" name="duration">
                                        <option <?php if(strcmp($duration, 'one') == 1) {echo "selected";} ?>>one</option>
                                        <option <?php if(strcmp($duration, 'two') == 1) {echo "selected";} ?>>two</option>
                                        <option <?php if(strcmp($duration, 'three') == 1) {echo "selected";} ?>>three</option>
                                        <option <?php if(strcmp($duration, 'four') == 1) {echo "selected";} ?>>four</option>
                                        <option <?php if(strcmp($duration, 'five') == 1) {echo "selected";} ?>>five</option>
                                    </select>
                                </div>

                          </div>

                          <div class="w3-container w3-margin w3-right">
                              <?php
                                echo "<button type='submit'  name='modif' value='$id' class='w3-button w3-green'>Update</button>";
                              ?>
                          </div>
                        </form>
                  </div>

                <?php          
            }
        ?>
        <br></br>
    </div>
</html>